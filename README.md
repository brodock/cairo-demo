# Dependencies

```
bundle install
brew install libmagic
```

# Demo

To run the demo type this:

```
bundle exec ruby opengraph.rb
```
