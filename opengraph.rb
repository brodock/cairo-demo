# frozen_string_literals: true
require 'rubygems'
require 'cairo'
require 'pango'
require 'rsvg2'
require 'stringio'
require 'pry'


GITLAB_LOGO = 'gitlab.svg'
GITLAB_LOGO_HEIGHT = 48
IMAGE_EXTENSION = '.png'
OPENGRAPH_WIDTH = 1200
OPENGRAPH_HEIGHT = 600 #630
PROJECT_LOGO_HEIGHT = 200
MARGIN = 80
TEXT_MARGIN = 20
DESCRIPTION_Y_POSITION = 220
LANGUAGES = {
  ruby: '#701516',
  html: '#e34c26',
  shell: '#89e051',
  dockerfile: '#384d54',
  css: '#563d7c'
}
LANGUAGE_BAR_HEIGHT = 30 # height of the bar
LANGUAGE_BAR_MIN_WIDTH = 10 # minimum visual representation
Language = Struct.new(:language, :percentage)

def rounded_rectangle(x, y, width, height, curvature_radius, context:)
  aspect = 1.0
  corner_radius = height/curvature_radius
  radius = corner_radius / aspect
  degrees = Math::PI / 180.0

  context.new_sub_path
  context.arc(x + width - radius, y + radius, radius, -90 * degrees, 0 * degrees)
  context.arc(x + width - radius, y + height - radius, radius, 0 * degrees, 90 * degrees)
  context.arc(x + radius, y + height - radius, radius, 90 * degrees, 180 * degrees)
  context.arc(x + radius, y +  radius, radius, 180 * degrees, 270 * degrees)
  context.close_path
end

def resize(surface, width, height)
  # resize the image
  resized = Cairo::ImageSurface.new(width, height)
  ctx = Cairo::Context.new(resized)

  ctx.scale(width.to_f/surface.width.to_f, height.to_f/surface.height.to_f)
  ctx.set_source(surface)
  ctx.paint

  ctx.destroy
  surface.finish

  resized
end

def gitlab_logo(size)
  rsvg = RSVG::Handle.new_from_data(IO.read(GITLAB_LOGO))

  scale_proportion = size.to_f/rsvg.width

  surface = Cairo::ImageSurface.new(:argb32, size, size)
  context = Cairo::Context.new(surface)

  context.scale(scale_proportion, scale_proportion)
  context.render_rsvg_handle(rsvg)
  context.destroy

  surface
end

def render_project(namespace, project, project_description, logo_file, languages)
  output = StringIO.new

  Cairo::ImageSurface.new(OPENGRAPH_WIDTH, OPENGRAPH_HEIGHT) do |surface|
    ctx = Cairo::Context.new(surface)

    # white background
    ctx.set_source_color("#fff")
    ctx.paint

    # add project logo
    logo_surface = resize(Cairo::ImageSurface.from_png(logo_file), PROJECT_LOGO_HEIGHT, PROJECT_LOGO_HEIGHT)

    source_x = surface.width-logo_surface.width-MARGIN
    source_y = MARGIN

    # apply a rectangle with rounded courners clip mask
    ctx.save
    rounded_rectangle(source_x, source_y, PROJECT_LOGO_HEIGHT, PROJECT_LOGO_HEIGHT, 10.0, context: ctx)
    ctx.clip
    ctx.set_source(logo_surface, source_x, source_y)
    ctx.paint
    logo_surface.finish
    ctx.restore

    # add project name
    layout = ctx.create_pango_layout
    layout.font_description = Pango::FontDescription.new(%Q{Roboto,"Apple Color Emoji" 52})
    layout.markup = %Q(<span foreground="#303030" font_weight="thin">#{namespace} / </span><span foreground="#303030" font_weight="bold">#{project}</span>)
    layout.width = 800 * Pango::SCALE

    ctx.move_to(MARGIN+TEXT_MARGIN, MARGIN+TEXT_MARGIN)
    ctx.show_pango_layout(layout)

    # add project description
    layout = ctx.create_pango_layout
    layout.font_description = Pango::FontDescription.new("Roboto 24")
    layout.markup = %Q(<span foreground="#bcbcbc">#{project_description}</span>)
    layout.width = 800 * Pango::SCALE

    ctx.move_to(MARGIN+TEXT_MARGIN, MARGIN+DESCRIPTION_Y_POSITION)
    ctx.show_pango_layout(layout)

    # add gitlab logo
    logo_surface = gitlab_logo(GITLAB_LOGO_HEIGHT)

    source_x = surface.width-logo_surface.width-MARGIN
    source_y = surface.height-logo_surface.height-MARGIN
    ctx.set_source(logo_surface, source_x, source_y)
    ctx.paint
    logo_surface.finish

    # language bar
    bar_start_x = 0
    bar_width = (surface.width - (LANGUAGE_BAR_MIN_WIDTH*languages.count))
    languages.each do |lang|
      bar_size = (bar_width.to_f/100)*lang.percentage + LANGUAGE_BAR_MIN_WIDTH

      ctx.move_to(bar_start_x, surface.height-LANGUAGE_BAR_HEIGHT)
      ctx.set_source_color(LANGUAGES[lang.language])
      ctx.rectangle(bar_start_x, surface.height-LANGUAGE_BAR_HEIGHT, bar_size, LANGUAGE_BAR_HEIGHT)
      ctx.fill

      bar_start_x += bar_size
    end

    # save
    #ctx.target.write_to_png(output)
    surface.write_to_png(output)
  end

  output.rewind
  File.open("output.png", "wb") do |f|
    f.print(output.read)
  end
  output.truncate(0)
end

languages = [
  Language.new(:ruby, 86.39),
  Language.new(:html, 10.5),
  Language.new(:shell, 3.0),
  Language.new(:dockerfile, 0.1),
  Language.new(:css, 0.1)
]

render_project('gitlab-org', 'omnibus-gitlab', 'This project creates full-stack platform-specific downloadable packages for GitLab.', 'logo.png', languages)
#render_project('RubyFloripa', 'catalogo_empresas', "logo.png")
